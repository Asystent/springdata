package pl.com.bohdziewicz.tutorials.purespring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {

        ApplicationContext annotationConfigApplicationContext =
                new AnnotationConfigApplicationContext(DatabaseConfiguration.class);
    }
}
