package pl.com.bohdziewicz.tutorials.purespring;

import java.math.BigDecimal;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StartingBean {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public StartingBean(EmployeeRepository employeeRepository) {

        this.employeeRepository = employeeRepository;
    }

    @PostConstruct
    public void runAtStart() {

        Employee employee = new Employee();
        employee.setFirstName("withOutSpringBoot");
        employee.setLastName("Anonim");
        employee.setSalary(new BigDecimal(2000));

        employeeRepository.save(employee);

        Iterable<Employee> employeesWithFirstNameEqualRobert = employeeRepository.findByFirstName("withOutSpringBoot");

        if (employeesWithFirstNameEqualRobert.iterator().hasNext()) {
            Employee employeeFromEmployees = employeesWithFirstNameEqualRobert.iterator().next();
            System.out.println("I found: " + employeeFromEmployees);
        }
    }
}

