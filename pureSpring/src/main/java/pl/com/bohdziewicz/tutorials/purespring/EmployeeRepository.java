package pl.com.bohdziewicz.tutorials.purespring;

import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    Iterable<Employee> findByFirstName(String firstName);
}
