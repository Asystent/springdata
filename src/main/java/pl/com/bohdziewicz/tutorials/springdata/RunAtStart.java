package pl.com.bohdziewicz.tutorials.springdata;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class RunAtStart {

    private final EmployeeRepository employeeRepository;
    private final ToolsForThisProject toolsForThisProject;

    public RunAtStart(EmployeeRepository employeeRepository, ToolsForThisProject toolsForThisProject) {

        this.employeeRepository = employeeRepository;
        this.toolsForThisProject = toolsForThisProject;
    }

    @PostConstruct
    public void runAtStart() {

        toolsForThisProject.generateEmployee(100);
        log.info(">>>>>>>>>> EMPLOYEES <<<<<<<<<<<<:");
        toolsForThisProject.printAll(employeeRepository.findAll());


    }
}

