package pl.com.bohdziewicz.tutorials.springdata;

import java.util.List;

import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class ToolsForThisProject {

    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_RESET = "\u001B[0m";
    private final EmployeeRepository employeeRepository;
    private final EmployeeGenerator employeeGenerator;

    public ToolsForThisProject(EmployeeRepository employeeRepository, EmployeeGenerator employeeGenerator) {

        this.employeeRepository = employeeRepository;
        this.employeeGenerator = employeeGenerator;
    }

    void generateEmployee(long nEmployee) {

        if (employeeRepository.count() < nEmployee) {
            long missingEmployee = nEmployee - employeeRepository.count();
            log.warn("In DB missing " + missingEmployee
                    + " employees. I need generate them.");
            for (int i = 0; i < missingEmployee; i++) {
                employeeRepository.save(employeeGenerator.genereteEMploye());
            }
        }
    }

    String formatMessage(String message) {

        return ANSI_RED + message + ANSI_RESET;
    }

    void printAll(List<Employee> allUnsorted) {

        allUnsorted.forEach(log::info);
    }
}
