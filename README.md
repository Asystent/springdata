##### Kurs Artura Owczarka - Spring Data
Tu [> do obejrzenia.](https://www.youtube.com/playlist?list=PLU2dl_1LV_SR9VnEtO4s0uUI_WSmzDq1I)

###### Start: 05.06.2019r. 


- [x] Wstęp do Spring Data
- [x] Pierwszy projekt
- [x] Projekt bez Spring Boota
- [x] Rodzaje repozytoriów
- [x] Sortowanie i stronnicowanie
- [x] Własne metody w repozytoriach
- [x] Zapytania natywne i JPQL
- [x] Więcej o tworzeniu repozytoriów
- [x] Modyfikowanie danych
- [x] Audyt danych

![](http://bohdziewicz.com.pl/imagesForGit/coffee_todo.jpg)
