![#1589F0](https://placehold.it/15/1589F0/000000?text=-)`2019.06.21`

End of tutorial. 
There were lots of useful things, but I noticed that it is easy to learn but it is easy to forget :( 

So: most important - **repeat and practice**! :thumbsup:

See you later. 

![#1589F0](https://placehold.it/15/1589F0/000000?text=-)`2019.06.14`

Start next film.

![#1589F0](https://placehold.it/15/1589F0/000000?text=-)`2019.06.14`

Short film about replacing CrudRepository with JpaRepository.

![#1589F0](https://placehold.it/15/1589F0/000000?text=-)`2019.06.14`
Third film.

![#1589F0](https://placehold.it/15/1589F0/000000?text=-)`2019.06.14`

Short break. I have work to do.  

![#1589F0](https://placehold.it/15/1589F0/000000?text=-)`2019.06.08/09`

Oh! I just got away with connecting to MySql in docker (and docer was runned on Virtual Machine (VMVare) with Ubuntu).
The first time it is no so easy. Lots of configuration. 

In application.properties you have no magic:
```java
spring.datasource.url=jdbc:mysql://192.168.162.129:3306/mysql?serverTimezone=Europe/Warsaw&useSSL=False
spring.datasource.username=root
spring.datasource.password=my-secret-pw
```

On Vmware on Ubuntu you have to install docker. And then pull image of mysql server. 
```shell
docker pull mysql
docker run -p 3306:3306 --name some-mysql2 -e MYSQL_ROOT_PASSWORD=my-secret-pw -d  mysql
```
Very important is swich -p. Without it mysqls port will be on virtual network (network of docker), not on VM.  

You have to login to docker, then to mysql.
```shell
docker exec -it some-mysql2 bash
mysql -p
``` 
then add privileges,
```sql
ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'my-secret-pw';FLUSH PRIVILEGES;
```
and check database name:
```sql
show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
4 rows in set (0.00 sec)
```
On ubuntu you should open port:
```
ufw open 3306/tcp
``` 
And its all :thumbsup:

Maybe one bonus. If you want to look at mysql logs live, you should:

```sql
SET GLOBAL general_log = 'ON';
SHOW VARIABLES LIKE "general_log%";
+------------------+---------------------------------+
| Variable_name    | Value                           |
+------------------+---------------------------------+
| general_log      | ON                              |
| general_log_file | /var/lib/mysql/059a7a292732.log |
+------------------+---------------------------------+
```
and then:
```
tail -f -n300 /var/lib/mysql/059a7a292732.log
```
:thumbsup:

![#f03c15](https://placehold.it/15/f03c15/000000?text=+) `2019.06.08`

### :bug: Bug  in Owczareks tutorial. 

It's bug in module pureSpring as well as in main module. If you use properties
```java
properties.setProperty("hibernate.hbm2ddl.auto", "create");
``` 
every time when you start the program, it will drop old table in db and create new one. I figured it out, because I had to turn on MySql logging (I was thinking that is something wrong with @GeneretedValue, and I was looking for clue). 

Solution:
```java
properties.setProperty("hibernate.hbm2ddl.auto", "update");
```

And if you want to turn on logging mentioned above, you have to use:

```java
spring.jpa.show-sql=true
spring.jpa.properties.hibernate.format_sql=true
logging.level.org.hibernate.SQL=DEBUG
logging.level.org.hibernate.type.descriptor.sql.BasicBinder=TRACE
logging.level.org.springframework.jdbc.core.JdbcTemplate=DEBUG
logging.level.org.springframework.jdbc.core.StatementCreatorUtils=TRACE
```  

Ufff. It was long day. 

----

![#1589F0](https://placehold.it/15/1589F0/000000?text=+) `08.06.2019`

End of second film. You can find comments in pull request. 

![#1589F0](https://placehold.it/15/1589F0/000000?text=+) `08.06.2019`

Project for new lesson (new film) adjusted. 

![#1589F0](https://placehold.it/15/1589F0/000000?text=+) `07.06.2019`


New 'modus operandi'. From now, all changes will be push only on gitlab. At the end of project I will push everything on github.

Every change to branch 'Lessons' will be require pull request to master (where I marks some comments.)

Pushing to master will take place after end of each tutorial film. Then should be update logs.md file and readme.md file.

Let's try it out :)

By the way I have to try something (in experiment.md file)


![#1589F0](https://placehold.it/15/1589F0/000000?text=+) `06.06.2019`  
I have to revert this notes.

![#c5f015](https://placehold.it/15/c5f015/000000?text=+) `06.06.2019 17:21`

###### Problems still to be solved

Turned out that are some connections between local Gradle installation and version using in Intellij. When I updated Gradle (Windows) suddenly solve from above stopped working. 

```groovy
distributionUrl=https\://services.gradle.org/distributions/gradle-4.10-all.zip
```
This line has to be tied with local Gradle installation. Even though Intellij is set to don't use local Gradle installation.


![#1589F0](https://placehold.it/15/1589F0/000000?text=+) `06.06.2019`  

Lot of things to do during connecting with mysql database. More comments in code.
* allow connect to mydevil from any host
```sql
devil mysql access add (user)
```  

![#f03c15](https://placehold.it/15/f03c15/000000?text=+) `2019.06.06`

###### Problems with IDE
I've been struggling with this project for over 24 hours. And why? Because Intellij does not cooperate with gradle. I still don't know solution for this bug, but found temporary fix. 
```groovy
distributionBase=GRADLE_USER_HOME
distributionPath=wrapper/dists
zipStoreBase=GRADLE_USER_HOME
zipStorePath=wrapper/dists
distributionUrl=https\://services.gradle.org/distributions/gradle-4.10-all.zip
```
With this settings and using Gradle 'wrapper' configuration - suddenly everything backs to normal. 




  